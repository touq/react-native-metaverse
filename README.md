
# react-native-metaverse

## Getting started

`$ yarn add git+ssh://git@github.com:Vyoo/react-native-metaverse.git`

### Mostly automatic installation

`$ react-native link react-native-metaverse`

(don't forget to do Final installation steps below)

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-metaverse` and add `RNMetaverse.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNMetaverse.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.wirestorm.metaverse.RNMetaversePackage;` to the imports at the top of the file
  - Add `new RNMetaversePackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-metaverse'
  	project(':react-native-metaverse').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-metaverse/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-metaverse')
  	```

## Final installation steps

#### iOS

##### Prerequisite

1. Target iOS 9.3 or higher
2. Create a Metaverse account and log in to Metaverse Studio
3. Install git-lfs (this is a REQUIRED step!)

##### Installation steps

1. Open terminal -> project root -> ios -> ```pod init```
2. Edit the pod file like this:
   ```
     source "https://github.com/CocoaPods/Specs.git"
     source "https://github.com/GoMeta/meta-ios-pods"

     platform :ios, "9.3"
     use_frameworks!

     target 'your_package_name' do
       pod 'RNMetaverse', path: '../node_modules/react-native-metaverse/ios'
     end
   ```
3. Install -> ```pod install --repo-update```
4. Open .xcworkspace -> Pods -> Target and update Build settings -> Swift language compiler to 4.3 for the following packages. 1) ```Alamofire``` 2) ```SwiftyJSON``` 3) ```Zip```

##### Configure App Privacy Settings
  Metaverse experiences depend heavily on access to the Camera and device motion. Camera scenes that record video also require access to the microphone. Make sure your app's Info.plist file contains the following:

```
  <key>NSCameraUsageDescription</key>
  <string>Use your camera to experience augmented reality</string>
  <key>NSMicrophoneUsageDescription</key>
  <string>Use your microphone to record audio</string>
  <key>NSMotionUsageDescription</key>
  <string>Use device motion to experience augmented reality</string>
```

##### More info
https://github.com/GoMeta/meta-ios-sdk

#### Android

1. Open up `android/app/build.gradle` 
  - Set `compileSdkVersion` and `targetSdkVersion` to 27 or above.
  - Set `minSdkVersion` to 19 or above.
  - Set `buildToolsVersion` to 27.0.3 or above.
```groovy
android {
    compileSdkVersion 27
    buildToolsVersion '27.0.3'
    ...

    defaultConfig {
        minSdkVersion 19
        targetSdkVersion 27
        ...
    }
    ...
```
2. Open up `android/build.gradle` 
- Add this `maven { url 'http://maven2.gometa.io' }` to `allprojects.repositories`:
```groovy
allprojects {
    repositories {
        mavenLocal()
        jcenter()
        maven { url 'http://maven2.gometa.io' } 
        ...
    }
}
```

## Usage
```javascript
import RNMetaverse from 'react-native-metaverse';

// initialize the metaverse sdk
componentDidMount(){
  const apiKey = 'api-test-7bc6df9bf5ec8aaf477d4b0bc5a57138a9b5dd45cf9df1e7';
  RNMetaverse.initialise(apiKey);
}

// Load and show storyboard
const storyboardId = 'db1f017b-591a-4029-809a-0f0ef31f1584';
RNMetaverse.launchStoryboard(storyboardId);
```
