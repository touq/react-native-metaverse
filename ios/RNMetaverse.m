
#import "RNMetaverse.h"

@import Meta;

@implementation RNMetaverse

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}
RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(launchStoryboard:(NSString *) storyboardId)
{
  MetaExperience *experience = [[MetaExperience alloc] initWithId:storyboardId];
    [[Meta shared] presentWithExperience: experience];
}

RCT_EXPORT_METHOD(initialise:(NSString *) configureKey)
{
  [Meta setLogLevel: MetaLogLevelDebug];
    [Meta configureWithKey: configureKey];
}

@end
