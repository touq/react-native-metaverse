package com.wirestorm.metaverse;

import android.app.Activity;
import android.content.Intent;
import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import io.gometa.metaverse.storyboard.sdk.Meta;
import org.jetbrains.annotations.NotNull;

public class RNMetaverseModule extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;

  public RNMetaverseModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
    reactContext.addActivityEventListener(new ActivityEventListener() {
      @Override public void onActivityResult(
          Activity activity, int requestCode, int resultCode, Intent data) {
        if (requestCode == 1000) {
          String storyboardId = null;
          String cause = null;
          if (data != null) {
            storyboardId = data.getStringExtra(Meta.StoryboardResult.STORYBOARD_ID);
            if (data.hasExtra(Meta.StoryboardResult.CLOSE_REASON)) {
              cause = Integer.toString(data.getIntExtra(Meta.StoryboardResult.CLOSE_REASON, -1));
            }
          }
          StringBuilder sb = new StringBuilder("Storyboard ").append(storyboardId);
          if (resultCode == Activity.RESULT_CANCELED) {
            sb.append(" CANCELED");
          } else {
            sb.append(" OK");
          }
          sb.append(" with cause: ").append(cause);
        }
      }

      @Override public void onNewIntent(Intent intent) {

      }
    });
  }

  @ReactMethod public void initialise(@NotNull final String apiKey) {
    android.os.Handler mainHandler = new android.os.Handler(reactContext.getMainLooper());
    Runnable myRunnable = new Runnable() {
      @Override public void run() {
        Meta.initialize(reactContext, apiKey);
      }
    };
    mainHandler.post(myRunnable);
  }

  @ReactMethod public void launchStoryboard(@NotNull final String storyboardId) {
    getCurrentActivity().startActivityForResult(
        Meta.getStoryboardIntent(reactContext, storyboardId), 1000, null);
  }

  @Override public String getName() {
    return "RNMetaverse";
  }
}
